package ie.ul.android.lab_week9;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.IBinder;

public class FreeFallService extends Service {

	private static final int FREE_FALLING = 1;
	private boolean isFreeFalling = false;
	private boolean isPlaying = false;
	private static String FREEFALLINGSNIPPET = "fall.wav";

    SensorManager sensorManager;
	Sensor accelerometer;


    @Override
    public void onCreate() {
        super.onCreate();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    }

    @Override
    public void onDestroy() {
		sensorManager.unregisterListener(accSensorListener);
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
		accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		sensorManager.registerListener(accSensorListener, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        // replace below return statement with correct one
        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    private SensorEventListener accSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            float values[] = sensorEvent.values;

            double MagG = 0;

            for (int i = 0; i < 3; i += 1) {
                MagG += Math.pow(values[i], 2);
            }
            MagG = Math.sqrt(MagG);
            if (MagG < FREE_FALLING) {
                if (!isFreeFalling) {
                    isFreeFalling = true;
                    playFreeFalling();
                } else {
                    isFreeFalling = false;
                }
            }

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

	private void playFreeFalling(){
		MediaPlayer mp = new MediaPlayer();
		if(!isPlaying){
			try{
				AssetFileDescriptor descriptor = getAssets().openFd(FREEFALLINGSNIPPET);
				mp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
				mp.prepare();
				mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

					@Override
					public void onCompletion(MediaPlayer arg0) {
						isPlaying = false;

					}

				});
				mp.start();
				isPlaying = true;
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
}
